Download cip-core
==================

    git clone --branch master https://gitlab.com/cip-project/cip-core.git

Include EdgeX dependency layer
==============================
	
    Add the below three lines in the kas configuration file for the QEMU target
    vim ./cip-core/deby/poky/meta-cip-qemux86-64/kas-qemux86-64.yml
	
	meta-edgex-cip:
 	       url: "https://gitlab.com/cip-playground/edgex-cip/meta-edgex-cip.git"
               refspec: master

	add the above lines under repos header in the configuration file.

Build the binaries for QEMU taraget
===================================

    Build using KAS:

    [Note]Make sure that you have docker installed on the host machine

    host$ docker run -v $PWD/cip-core:/cip-core -e USER_ID=`id -u $USER` -e http_proxy=$http_proxy -e https_proxy=$https_proxy -e NO_PROXY="$no_proxy" -it kasproject/kas:0.13.0 sh
    docker$ cd /cip-core/deby/poky/
    docker$ kas build --target core-image-minimal meta-cip-qemux86-64/kas-qemux86-64.yml

Run cip-core image on QEMU
==========================

    [Note] make sure that your KAS docker image includes /sbin/ip
    [Opt] host$ sudo modprobe tun
    host$ docker run -v /dev/net/tun:/dev/net/tun -v $PWD/cip-core:/cip-core -e USER_ID=`id -u $USER` -e http_proxy=$http_proxy -e https_proxy=$https_proxy -e NO_PROXY="$no_proxy" -it kasproject/kas:0.13.0 sh
    docker$ cd /cip-core/deby/poky/
    docker$ kas shell --target core-image-minimal meta-cip-qemux86-64/kas-qemux86-64.yml
    docker$ runqemu qemux86-64 nographic slirp qemuparams="-m 1024"

Verify EdgeX Foundary Demonstration API
=======================================

    1) start ssh and docker server
       [Note] if you are behind the proxy, please export the proxy variable to the environment
	cip$ /etc/init.d/ssh start
	cip$ /etc/init.d/docker start

    2) Download the compose file for the EdgeX services 
       For more details how to pull and run EdgeX microservices please check this link (https://nexus.edgexfoundry.org/content/sites/docs/staging/master/docs/_build/html/Ch-WalkthroughRunning.html)

	cip$ curl -k -o docker-compose.yml https://raw.githubusercontent.com/edgexfoundry/developer-scripts/master/compose-files/docker-compose-california-0.6.0.yml
	cip$ docker-compose pull

    3) Run the following commands to start the core, supporting and export micro services of EdgeX
	cip$ docker-compose up -d volume
	cip$ docker-compose up -d consul
	cip$ docker-compose up -d config-seed
	cip$ docker-compose up -d mongo
	cip$ docker-compose up -d logging
	cip$ docker-compose up -d notifications
	cip$ docker-compose up -d metadata
	cip$ docker-compose up -d data
	cip$ docker-compose up -d command
	cip$ docker-compose up -d scheduler
	cip$ docker-compose up -d export-client
	cip$ docker-compose up -d export-distro
	cip$ docker-compose up -d rulesengine
	
    4) check all the services are up and started by using below command
	cip$ docker-compose ps

    5) now you can verify the EdgeX Demonostration as described in this link (https://nexus.edgexfoundry.org/content/sites/docs/staging/master/docs/_build/html/Ch-WalkthroughData.html)
	
	use the script file in folder script/cip_edgex_microsevices_walkthrough.sh that uses curl to verify the EdgeX Foundary API walkthrough.

	cip$ curl -k -o cip_edgex_microsevices_walkthrough.sh https://gitlab.com/cip-playground/edgex-cip/meta-edgex-cip/raw/master/script/cip_edgex_microsevices_walkthrough.sh
	cip$ chmod +x cip_edgex_microsevices_walkthrough.sh

	First, initialize the device and device service (one time only)
	cip$ ./cip_edgex_microsevices_walkthrough.sh init

	As there is no real Device or Device Service in this walk through, EdgeX doesn’t know that.
	Therefore, with all the configuration and setup you have performed, you can simulate device sending data to EdgeX and simlarly you can also read all the data set in Edgex.

	Use the below command to simulate the device data, that will send the humancount value with the value provided in the argument
	cip$ ./cip_edgex_microsevices_walkthrough.sh set 100

	read the data
	cip$ ./cip_edgex_microsevices_walkthrough.sh read

