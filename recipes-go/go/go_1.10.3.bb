
SUMMARY = "go"
DESCRIPTION = "\
	The Go programming language is an open source project to \
	make programmers more productive.\
	Go is expressive, concise, clean, and efficient.\
	Its concurrency mechanisms make it easy to write programs \
	that get the most out of multicore and networked machines \
	while its novel type system enables flexible and modular \
	program construction. Go compiles quickly to machine code \
	yet has the convenience of garbage collection and the power \
	of run-time reflection. It's a fast, statically typed, \
	compiled language that feels like a dynamically typed, \
	interpreted language.\
	"
HOMEPAGE = "https://golang.org"
	
LICENSE = "CLOSED"

SRC_URI += "https://dl.google.com/go/go${PV}.linux-amd64.tar.gz"
SRC_URI += "file://go-bin-path.sh"
SRC_URI[sha256sum] = "fa1b0e45d3b647c252f51f5e1204aba049cde4af177ef9f2181f43004f901035"


S = "${WORKDIR}/"


do_install () {

	#create directory /usr/local/go/
	install -d ${D}${prefix}/local/go

	#copy the go binary to the target location
	cp -r ${WORKDIR}/go/api ${D}${prefix}/local/go/
	cp -r ${WORKDIR}/go/bin ${D}${prefix}/local/go/
	cp -r ${WORKDIR}/go/lib ${D}${prefix}/local/go/
	cp -r ${WORKDIR}/go/blog ${D}${prefix}/local/go/
	cp -r ${WORKDIR}/go/misc ${D}${prefix}/local/go/
	cp -r ${WORKDIR}/go/pkg ${D}${prefix}/local/go/

	#add go path in environment variable
	install -d ${D}${sysconfdir}/profile.d
	install -m 0755 ${WORKDIR}/go-bin-path.sh ${D}${sysconfdir}/profile.d/
}

FILES_${PN} += " \
	${sysconfdir}/profile.d/go-bin-path.sh \
        ${prefix}/local/go/* \
"

INSANE_SKIP_${PN} += "ldflags"
INSANE_SKIP_${PN} += "already-stripped"
INSANE_SKIP_${PN} += "staticdev"
