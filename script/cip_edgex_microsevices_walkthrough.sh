#!/bin/sh


#The steps for verification of these services in Postman are available at https://wiki.edgexfoundry.org/display/FA/EdgeX+Demonstration+API+Walk+Through
#This scrpt is for verification of EdgeX microservices, in general we use Postman for verifying RESTFull services like EdgeX, 
#but on QEMU browser is not available so we have used this script for verification of EdgeX microservices
#Any POST call does not return anything, only GET calls return data which should be verified



createAddressable()
{
	echo ""
	echo "############################################"
	echo "#     Creating addressable"
	echo "###########################################"
	echo ""

	#POST to http://localhost:48081/api/v1/addressable
	#BODY: {"name":"camera control","protocol":"HTTP","address":"172.17.0.1","port":49977,"path":"/cameracontrol","publisher":"none","user":"none","password":"none","topic":"none"}

	echo "creating addressable for device service...."
	curl -x "" -X POST -H "Content-Type:application/json" http://$host:48081/api/v1/addressable -d ' {"name":"camera control","protocol":"HTTP","address":"172.17.0.1","port":49977,"path":"/cameracontrol","publisher":"none","user":"none","password":"none","topic":"none"}'

	#POST to http://localhost:48081/api/v1/addressable
	#BODY: {"name":"camera1 address","protocol":"HTTP","address":"172.17.0.1","port":49999,"path":"/camera1","publisher":"none","user":"none","password":"none","topic":"none"}

	echo "creating addressable for device...."
	curl -x "" -X POST -H "Content-Type:application/json" http://$host:48081/api/v1/addressable -d '{"name":"camera1 address","protocol":"HTTP","address":"172.17.0.1","port":49999,"path":"/camera1","publisher":"none","user":"none","password":"none","topic":"none"}'

}


createValueDescriptors()
{

	echo ""
	echo "############################################"
	echo "#     create Value Descriptors "
	echo "############################################"
	echo ""

	#POST to http://localhost:48080/api/v1/valuedescriptor
	#BODY:  {"name":"humancount","description":"people count", "min":"0","max":"100","type":"I","uomLabel":"count","defaultValue":"0","formatting":"%s","labels":["count","humans"]}

	echo "creating valuedescriptor human count...."
	curl -x ""  -X POST -H "Content-Type:application/json" http://$host:48080/api/v1/valuedescriptor -d '{"name":"humancount","description":"people count", "min":"0","max":"100","type":"I","uomLabel":"count","defaultValue":"0","formatting":"%s","labels":["count","humans"]}'

	#POST to http://localhost:48080/api/v1/valuedescriptor
	#BODY:  {"name":"caninecount","description":"dog count", "min":"0","max":"100","type":"I","uomLabel":"count","defaultValue":"0","formatting":"%s","labels":["count","canines"]}

	echo "creating valuedescriptor caninecount...."
	curl -x ""  -X POST -H "Content-Type:application/json" http://$host:48080/api/v1/valuedescriptor -d '{"name":"caninecount","description":"dog count", "min":"0","max":"100","type":"I","uomLabel":"count","defaultValue":"0","formatting":"%s","labels":["count","canines"]}'

	#POST to http://localhost:48080/api/v1/valuedescriptor
	#BODY:  {"name":"depth","description":"scan distance", "min":"1","max":"10","type":"I","uomLabel":"feet","defaultValue":"1","formatting":"%s","labels":["scan","distance"]}

	echo "creating valuedescriptor depth...."
	curl -x "" -X POST -H "Content-Type:application/json" http://$host:48080/api/v1/valuedescriptor -d '{"name":"depth","description":"scan distance", "min":"1","max":"10","type":"I","uomLabel":"feet","defaultValue":"1","formatting":"%s","labels":["scan","distance"]}'

	#POST to http://localhost:48080/api/v1/valuedescriptor
	#BODY:  {"name":"duration","description":"time between events", "min":"10","max":"180","type":"I","uomLabel":"seconds","defaultValue":"10","formatting":"%s","labels":["duration","time"]}

	echo "creating valuedescriptor duration...."
	curl -x "" -X POST -H "Content-Type:application/json" http://$host:48080/api/v1/valuedescriptor -d '{"name":"duration","description":"time between events", "min":"10","max":"180","type":"I","uomLabel":"seconds","defaultValue":"10","formatting":"%s","labels":["duration","time"]}'

	#POST to http://localhost:48080/api/v1/valuedescriptor
	#BODY:  {"name":"cameraerror","description":"error response message from a camera", "min":"","max":"","type":"S","uomLabel":"","defaultValue":"error","formatting":"%s","labels":["error","message"]}

	echo "creating valuedescriptor cameraerror...."
	curl -x "" -X POST -H "Content-Type:application/json" http://$host:48080/api/v1/valuedescriptor -d '{"name":"cameraerror","description":"error response message from a camera", "min":"","max":"","type":"S","uomLabel":"","defaultValue":"error","formatting":"%s","labels":["error","message"]} '
}


AddingDeviceProfile(){
	echo ""
	echo "############################################"
	echo "#    Upload Device Profile"
	echo "############################################"
	echo ""
	#POST to http://localhost:48081/api/v1/deviceprofile/uploadfile
	#No headers
	#FORM-DATA:
	#key:  "file"
	#value:  CameraMonitorProfile.yml

	curl -k -o CameraMonitorProfile.yml "https://wiki.edgexfoundry.org/download/attachments/2785644/CameraMonitorProfile.yml?version=3&modificationDate=1499616437000&api=v2"

	[ ! -e CameraMonitorProfile.yml ] && echo "Download profile not exisit" && return

	curl -x "" -F 'file=@CameraMonitorProfile.yml' http://$host:48081/api/v1/deviceprofile/uploadfile
}


registerDeviceService()
{
	echo ""
	echo "############################################"
	echo "#    Registering device service"
	echo "############################################"
	echo ""
	#http://localhost:48081/api/v1/deviceservice
	#BODY {"name":"camera control device service","description":"Manage human and dog counting cameras","labels":["camera","counter"],"adminState":"unlocked","operatingState":"enabled","addressable":{"name":"camera control"}}

	curl -x "" -X POST -H "Content-Type:application/json" http://$host:48081/api/v1/deviceservice -d '{"name":"camera control device service","description":"Manage human and dog counting cameras","labels":["camera","counter"],"adminState":"unlocked","operatingState":"enabled","addressable":{"name":"camera control"}} '

}

provisionDevice()
{
	echo ""
	echo "############################################"
	echo "#    Provision device"
	echo "############################################"
	echo ""
	#POST to http://localhost:48081/api/v1/device
	#BODY:  {"name":"countcamera1","description":"human and dog counting camera #1","adminState":"unlocked","operatingState":"enabled","addressable":{"name":"camera1 address"},"labels":["camera","counter"],"location":"","service":{"name":"camera control device service"},"profile":{"name":"camera monitor profile"}}

	curl -x "" -X POST -H "Content-Type:application/json" http://$host:48081/api/v1/device -d '{"name":"countcamera1","description":"human and dog counting camera #1","adminState":"unlocked","operatingState":"enabled","addressable":{"name":"camera1 address"},"labels":["camera","counter"],"location":"","service":{"name":"camera control device service"},"profile":{"name":"camera monitor profile"}} '

}

TestSetup()
{
	#Testing the setup
	echo ""
	echo "###########################"
	echo "#    Testing the setup!!!"
	echo "###########################"
	echo ""

	echo "Check the Device Service:"
	echo "========================="
	curl -x "" http://$host:48081/api/v1/deviceservice

	curl -x ""  http://$host:48081/api/v1/deviceservice/label/camera 

	echo "check the Device"
	echo "================"

	curl -x "" http://$host:48081/api/v1/device

	curl -x "" http://$host:48081/api/v1/device/profilename/camera%20monitor%20profile

	echo "Check the Commands:"
	echo "==================="
	curl -x "" http://$host:48082/api/v1/device/name/countcamera1

	echo "Check the Value Descriptors:"
	echo "============================"
	curl -x "" http://$host:48080/api/v1/valuedescriptor

	curl -x "" http://$host:48080/api/v1/event/count
}


SendEvent()
{
	#Send event to Core Data
	echo ""
	echo "simulating camera device sent the data to EdgeX"

	curl -x "" -X POST -H "Content-Type:application/json" http://$host:48080/api/v1/event -d '{"device":"countcamera1","origin":1471806386919, "readings":[{"name":"humancount","value":"'$1'","origin":1471806386919},{"name":"caninecount","value":"0","origin":1471806386919}]}'
	echo ""
}

ReadData()
{
	echo "Read Data from DB in EdgeX"

	count=$(curl -s -x "" http://$host:48080/api/v1/event/count)
	echo "Total count: $count"
	echo ""
	#curl -x "" http://$host:48080/api/v1/event/device/countcamera1/10
	curl -x "" http://$host:48080/api/v1/reading/name/humancount/$count
	echo ""
}

#Register an Export Client
#curl -x "" http://$host:48071/api/v1/registration

usage()
{
	echo ""
	echo "Usage:"
	echo "	$0 command [value]"
	echo ""
	echo "	 command: init | test | set [value] | read"
	echo ""

	exit 0
}

host=127.0.0.1
cmd=$1

[ "$cmd" == "" ] && usage

if [ "$cmd" == "init" ]; then

	createAddressable
	createValueDescriptors
	AddingDeviceProfile
	registerDeviceService
	provisionDevice

elif [ "$cmd" == "test" ]; then

	TestSetup

elif [ "$cmd" == "set" ]; then

	[ $2 == "" ] && usage
	SendEvent $2

elif [ "$cmd" == "read" ]; then

	ReadData

else
	usage
fi


