
SUMMARY = "Docker"
DESCRIPTION = "\
	A Docker container image is a lightweight, standalone, executable \
	package of software that includes everything needed to run an application: code, \
 	runtime, system tools, system libraries and settings. \
	Container images become containers at runtime and in the case of Docker containers \
	- images become containers when they run on Docker Engine. Available for both Linux \
	and Windows-based applications, containerized software will always run the same, \
	regardless of the infrastructure. Containers isolate software from its environment \
	and ensure that it works uniformly despite differences for instance between development \
	and staging. \
"

HOMEPAGE = "https://www.docker.com"

#LICENSE = "Apache-2.0"
LICENSE = "CLOSED"

SRC_URI += "https://download.docker.com/linux/static/stable/x86_64/docker-${PV}-ce.tgz"
SRC_URI[md5sum] = "9088b60cd39fd02fc0f639e9bacbdc19"
SRC_URI[sha256sum] = "0e245c42de8a21799ab11179a4fce43b494ce173a8a2d6567ea6825d6c5265aa"

SRC_URI += "file://docker_script"


S = "${WORKDIR}/docker"

do_install () {

	#copy the docker binary to /usr/bin
	install -d ${D}${bindir}
	install -m 0755 ${S}/* ${D}${bindir}
	install -d ${D}${sysconfdir}/docker

	#copy the start up script in /etc/init.d/
	install -d ${D}${sysconfdir}
	install -d ${D}${sysconfdir}/init.d/
	install -m 0755 ${WORKDIR}/docker_script ${D}${sysconfdir}/init.d/docker

}

FILES_${PN} += " \
        ${bindir}/docker \
        ${bindir}/dockerd \
        ${bindir}/docker-init \
        ${bindir}/docker-proxy \
        ${bindir}/docker-runc \
        ${bindir}/docker-containerd \
        ${bindir}/docker-containerd-ctr \
"

#Add the docker group
inherit useradd
USERADD_PACKAGES = "${PN}"
GROUPADD_PARAM_${PN} = "-r ${PN}"

INSANE_SKIP_${PN} = "already-stripped"
