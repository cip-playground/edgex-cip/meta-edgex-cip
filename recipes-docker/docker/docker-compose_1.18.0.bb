
SUMMARY = "Docker-compose"
DESCRIPTION = " \
	Compose is a tool for defining and running multi-container Docker applications. \
	With Compose, you use a YAML file to configure your application’s services. Then,\
	 with a single command, you create and start all the services from your configuration."
HOMEPAGE = "https://www.docker.com"

#LICENSE = "Apache-2.0"
LICENSE = "CLOSED"

SRC_URI += "https://github.com/docker/compose/releases/download/${PV}/docker-compose-Linux-x86_64"
SRC_URI[sha256sum] = "b2f2c3834107f526b1d9cc8d8e0bdd132c6f1495b036a32cbc61b5288d2e2a01"


do_install () {

	#copy the binary in /usr/bin/
        install -d ${D}${bindir}
        install -m 0755 ${WORKDIR}/docker-compose-Linux-x86_64 ${D}${bindir}/docker-compose
}

FILES_${PN} += " \
        ${bindir}/docker-compose \
"

INSANE_SKIP_${PN} = "already-stripped"

